import time
from pathlib import PurePath
from datetime import datetime
from urllib.parse import quote

from django.conf import settings
from django.core.files.storage import Storage
from django.utils.deconstruct import deconstructible

from office365.runtime.auth.client_credential import ClientCredential
from office365.sharepoint.client_context import ClientContext

from office365.runtime.queries.service_operation import ServiceOperationQuery
from office365.runtime.http.http_method import HttpMethod


SOURCE_URL_REPLACE_MAP = {
    "_": r"%5F",
    "-": r"%2D",
    ".": r"%2E",
    "~": r"%7E",
}


class FilenameWrapper:
    """
    Wraps a file object (or anything else) and
    passed through all attributes except for name
    (needed for large file uploads)
    """

    def __init__(self, new_name, file_object):
        self._new_name = new_name
        self._fo = file_object

    def __getattr__(self, attr):
        if attr == "name":
            return self._new_name
        return getattr(self._fo, attr)


"""
The default download_session in the O365 library doesn't return on write, meaning we can't stream the
contents to a response using an echo (like we could with csvwriter.write()). This is a soft modification
of File.download_session which attaches a .download_context property to the request 
"""


def _special_download_session(
    self, chunk_downloaded=None, chunk_size=1024 * 1024, use_path=True
):
    """
    :type file_object: typing.IO
    :type chunk_downloaded: (int)->None or None
    :type chunk_size: int
    :param bool use_path: Use Path instead of Url for addressing files
    """

    def _download_as_stream():
        qry = ServiceOperationQuery(self, "$value")

        def _construct_download_request(request):
            """
            :type request: office365.runtime.http.request_options.RequestOptions
            """
            request.stream = True
            request.method = HttpMethod.Get

        self.context.before_execute(_construct_download_request)

        def _process_download_response(response):
            """
            :type response: requests.Response
            """
            response.raise_for_status()

            # This is to be used by client implementation
            self.download_context = response

            # This will need to be implemented by the client
            # bytes_read = 0
            # for chunk in response.iter_content(chunk_size=chunk_size):
            #     bytes_read += len(chunk)
            #     if callable(chunk_downloaded):
            #         chunk_downloaded(bytes_read)
            #     file_object.write(chunk)

        self.context.after_execute(_process_download_response)
        self.context.add_query(qry)

    if use_path:
        self.ensure_property("ServerRelativePath", _download_as_stream)
    else:
        self.ensure_property("ServerRelativeUrl", _download_as_stream)
    return self


@deconstructible
class SharepointStorage(Storage):
    def __init__(
        self,
        client_id: str = None,
        client_secret: str = None,
        site_url: str = None,
        location: str = None,
        base_url=None,
    ):
        if not all([client_id, client_secret, site_url]):
            raise Exception("client_id, client_secret, and site_url are all required")
        if len(site_url.split("/")) != 5:
            raise Exception(
                'site_url must be in the form of "https://server/sites/SITENAME" with no trailing slash.'
            )

        self._client_id = client_id
        self._client_secret = client_secret
        self._site_url = site_url

        self._ctx = None
        # Make sure our root path is Shared Documents
        # I THINK that's required...
        self._location = PurePath("Shared Documents/")
        self._base_url = base_url
        if location is not None:
            o = PurePath(location)
            if o.parts[0] == "Shared Documents":
                self._location = o
            else:
                self._location /= o

    @property
    def ctx(self):
        if not self._ctx:
            client_credentials = ClientCredential(self._client_id, self._client_secret)
            self._ctx = ClientContext(self._site_url).with_credentials(
                client_credentials
            )
        return self._ctx

    def path(self, name):
        raise NotImplementedError("This backend doesn't support absolute paths.")

    def _full_path(self, name):
        local_path = PurePath(name)
        return self._location / local_path

    def _make_url(self, name):
        return self.ctx.create_safe_url(str(self._full_path(name)))

    def _get_file_by_name(self, name):
        return self.ctx.web.get_file_by_server_relative_url(self._make_url(name))

    def _save(self, orig_name, content):
        name = PurePath(orig_name)
        # Ensure the folder exists
        full_path = self._full_path(name)
        folder = self.ctx.web.ensure_folder_path(
            str(full_path.parents[0])
        ).execute_query()

        # Handle large uploaded files (which will be in temp!)
        # From https://github.com/vgrem/Office365-REST-Python-Client/blob/master/examples/sharepoint/files/upload_large_file.py
        if hasattr(content, "temporary_file_path"):
            size_chunk = 1000000
            with open(content.temporary_file_path(), "rb") as f:
                # Can't alter the name of an opened file context
                # so wrap it with the new filename
                # this prevents our files from being uploaded as
                # tmp_file_whatever.sfx
                fw = FilenameWrapper(full_path.name, f)
                folder.files.create_upload_session(fw, size_chunk).execute_query()
        # Handle normal in-memory files
        else:
            f = folder.upload_file(full_path.name, content.read()).execute_query()
        return str(name)

    # The following methods form the public API for storage systems, but with
    # no default implementations. Subclasses must implement *all* of these.

    def delete(self, name):
        # TODO: Untested
        self._get_file_by_name(name).delete_object().execute_query()

    def exists(self, name):
        try:
            return self._get_file_by_name(name).get().execute_query().exists
        except:
            return False
    
    def ensure_folder_exists(self, path):
        # Additional storage function not standard to django
        name = PurePath(path)
        full_path = self._full_path(name)
        folder = self.ctx.web.ensure_folder_path(
            str(full_path)
        ).execute_query()
        return True


    def listdir(self, path):
        full_path = self._full_path(path)
        folder = self.ctx.web.get_folder_by_server_relative_path(str(full_path))
        folder.expand(["Files", "Folders"]).get().execute_query()
        return [f.name for f in folder.folders], [f.name for f in folder.files]

    def size(self, name):
        return self._get_file_by_name(name).get().execute_query().length

    def download_to(self, name, file_object):
        return _special_download_session(self._get_file_by_name(name)).execute_query()

    def url(self, name):
        """
        This is going to return, by default, the ORG sharing url.
        That is, only individuals with existing access can download the item.

        if base_url is defined we'll instead serve locally.
        """

        if self._base_url:
            return self._base_url + "/" + str(name)
        source_url = quote(self._make_url(name), safe="")
        # quote skips ~-_. so let's do those ourselves
        for k, v in SOURCE_URL_REPLACE_MAP.items():
            source_url = source_url.replace(k, v)
        url = self._ctx.base_url + f"/_layouts/15/download.aspx?SourceUrl={source_url}"
        return url

    def get_accessed_time(self, name):
        return self.get_modified_time(name)

    def get_created_time(self, name):
        return datetime.fromisoformat(self._get_file_by_name(name).time_created)

    def get_modified_time(self, name):
        return datetime.fromisoformat(self._get_file_by_name(name).time_last_modified)
