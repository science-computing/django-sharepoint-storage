import os
from pathlib import PurePath
from urllib.parse import quote
import mimetypes


from django.core.exceptions import PermissionDenied
from django.http import Http404, StreamingHttpResponse
from django.views.generic import View


class Echo:
    def write(self, value):
        return value


def download_sharepoint_file(
    request, storage, path, content_disposition=None, filename=None
):
    buff = Echo()

    g = storage.download_to(path, buff).download_context

    def _r(buffer):
        bytes_read = 0
        for chunk in g.iter_content(chunk_size=1024 * 1024):
            bytes_read += len(chunk)
            yield buffer.write(chunk)

    response = StreamingHttpResponse(
        _r(buff), content_type=mimetypes.guess_type(PurePath(path).name)[0]
    )

    if content_disposition:
        response["Content-Disposition"] = "; ".join(
            [
                content_disposition.encode(),
                filename if filename else PurePath(path).name,
            ]
        )
    return response


class SharepointStorageView(View):
    storage = None

    #: The authorisation rule for accessing
    def can_access_file(self, path):
        return self.request.user.is_authenticated

    #: Whether the file should be displayed ``inline`` or show a download box (``attachment``).
    content_disposition = None

    #: Message to be displayed when the user cannot access the requested file.
    permission_denied_message = "Access denied"

    def get_path(self):
        return PurePath(self.kwargs["path"])

    def get(self, request, *args, **kwargs):
        """
        Handle incoming GET requests
        """
        path = self.get_path()

        if not self.can_access_file(path):
            raise PermissionDenied(self.permission_denied_message)

        if not self.storage.exists(path):
            raise Http404("File not Found")
        else:
            return self.serve_file(path)

    def serve_file(self, file_path):
        """
        Serve file from sharepoint through
        """
        return download_sharepoint_file(
            self.request, storage=self.storage, path=file_path
        )
