# Django Sharepoint Storage

If you're feeling like life just isn't complicated enough, perhaps you can spice things up by using Microsoft Sharepoint as a storage backend for Django. It's slow, convoluted, and generally not a great idea -- **But it works.**


## Note on Permissions
This library uses an API key for most of the IO. This means uploads and file listing use API permissions, not individual user permissions. File URLs will, by default, **a link to the file in sharepoint**, meaning users without sharepoint permissions won't be able to download the files.


To circumvent this see the **Custom File URLs** section below.


## Installation
You should be able to install this badboy via `pip install git+https://git.uwaterloo.ca/science-computing/django-sharepoint-storage`


If you want to pin a version you can toss in an `@vx.x.x` at the end (note the "v").


## Configuration
Here comes the messy bit. You'll need to have admin access to the sharepoint site you want to work with.

Log into the sharepoint in your web browser and head over to `https://your_sharepoint/sites/SITE/_layouts/15/appregnew.aspx`

From here you can create a new key -- the App Domain and Redirect URI don't matter for this project, so you can put in localhost. Save the generated ClientId and ClientSecret -- we'll need those!

With our clientID created we need to now grant it permissions at `https://your_sharepoint/sites/SITE/_layouts/15/appinv.aspx`

Paste your ClientId in there and hit lookup to auto-populate most of the fields. Now we only need to add some obscure XML to grant us permissions:

```xml
<AppPermissionRequests AllowAppOnlyPolicy="true">  
  <AppPermissionRequest Scope="http://sharepoint/content/sitecollection/web" 
   Right="FullControl" />
</AppPermissionRequests>
```

*Note: There may be better ways to set this permission to ensure the ClientID can ONLY read and write files, but I'm not an expert in this and the documentation is awful*

Once that's saved you can create new storages with the resulting content:

```py3
from sharepoint_storage.storage import SharepointStorage


storage = SharepointStorage(
    location="base_folder/",
    client_id="xxx",
    client_secret="xxx",
    site_url="xxx",
)
```
 

### Custom File URLs
By default download URLs point directly to sharepoint, which may not work if your users don't have access. You can set up custom file URLs which will cause all files to be downloaded through Django and allow you to manually control download permissions with a function.


To do this you will need to configure a custom view:

```py3
from sharepoint_storage.views import SharepointStorageView
from sharepoint_storage.storage import SharepointStorage

storage = SharepointStorage(
    ....
    base_url='/download/'
)

class DownloadView(SharepointStorageView):
    storage = storage
    def can_access_file(self, path):
        # Define your logic here. By default this will be self.request.user.is_authenticated
        return True
```

Then in your urls.py:

```py3
from django.urls import re_path
from your_views import DownloadView

urlpatterns = [
    ...
    re_path(r'^downloads/(?P<path>.*)$', DownloadView.as_view(), name='download_file'),
]
```
